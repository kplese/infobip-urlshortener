package com.infobip.urlshortener;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.RandomStringUtils;

import java.io.*;
import java.net.InetSocketAddress;
import java.nio.charset.Charset;
import java.sql.*;
import java.util.List;

/**
 * URL Shortener
 *
 */
public class App 
{
    private static Connection connection = null;
    private static int port = 0;

    public static void main( String[] args )
    {

        try {
            // Get the available port by passing 0 to InetSocketAddress constructor
            InetSocketAddress socketAddress = new InetSocketAddress(0);
            // Create and start the http server on selected port
            HttpServer server = HttpServer.create(socketAddress, 0);

            server.createContext("/", new UrlShortenerHandler());
            server.setExecutor(null); // creates a default executor
            server.start();

            port = server.getAddress().getPort();
            // Letting the user know the location where the service is running
            System.out.println("Service is running on following URL: http://localhost:" + server.getAddress().getPort());

        } catch(Exception e) {

        }


        try
        {
            // load the sqlite-JDBC driver using the current class loader
            Class.forName("org.sqlite.JDBC");

            // create a database connection
            connection = DriverManager.getConnection("jdbc:sqlite:urlshortener.db");
//            connection = DriverManager.getConnection("jdbc:sqlite::memory:");
            Statement statement = connection.createStatement();
            statement.setQueryTimeout(30);  // set timeout to 30 sec.

            statement.executeUpdate("drop table if exists account");
            statement.executeUpdate("create table account (account_id string UNIQUE, password string)");
            statement.executeUpdate("create table shorturls (url string UNIQUE, shorturl string, redirect_type string, counter integer default 0)");
            ResultSet rs = statement.executeQuery("select * from account");
        }
        catch(SQLException e)
        {
            System.err.println(e.getMessage());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

    }


    /**
     * Handle the HTTP requests and give appropriate responses
     */
    static class UrlShortenerHandler implements HttpHandler {
        @Override
        public void handle(HttpExchange t) throws IOException {

            InputStream is = t.getRequestBody();

            System.out.println("request header " + t.getRequestURI());

            BufferedReader streamReader = null;
            try {
                streamReader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            StringBuilder strBuilder = new StringBuilder();

            String inputStr;
            try {
                while ((inputStr = streamReader.readLine()) != null)
                    strBuilder.append(inputStr);
            } catch (IOException e) {
                e.printStackTrace();
            }

            inputStr = strBuilder.toString();

            if (t != null) {
                if (t.getRequestURI().toString().equalsIgnoreCase("/account")) {
                    handleAccount(t, inputStr);
                } else if (t.getRequestURI().toString().equalsIgnoreCase("/register")) {
                    handleRegister(t, inputStr);
                } else if (t.getRequestURI().toString().length() > 10 ) {
                    if(t.getRequestURI().toString().substring(0, 10).equalsIgnoreCase("/statistic")) {
                        handleStatistics(t);
                    }
                } else if (t.getRequestURI().toString().equalsIgnoreCase("/help")) {
                    handleHelp(t);
                } else {
                    handleRedirect(t);
                }
            }
        }

        private void handleAccount(HttpExchange t, String inputStr) {
            Gson gson = new Gson();
            JsonElement jelem = gson.fromJson(inputStr, JsonElement.class);

            JsonObject jobj = jelem.getAsJsonObject();

            String accountId = jobj.get("AccountId").getAsString();
            String password = RandomStringUtils.randomAlphanumeric(8);
            String response = "";
            JsonObject jsonObject = new JsonObject();

            try
            {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.

                statement.executeUpdate("insert into account values('"+ accountId +"', '"+ password +"')");
                ResultSet rs = statement.executeQuery("select * from account");
                while(rs.next())
                {
                    // read the result set
                    System.out.println("account_id = " + rs.getString("account_id"));
                    System.out.println("password = " + rs.getString("password"));

                }

            }
            catch(SQLException e)
            {
                // it probably means no database file is found
                System.err.println("ERROR handleAccount: " + e.getMessage());

                try {
                    jsonObject.addProperty("success", "false");
                    jsonObject.addProperty("description", "user already exists");

                    response = jsonObject.toString();

                    t.sendResponseHeaders(409, response.length());
                    OutputStream os = t.getResponseBody();
                    os.write(response.getBytes());
                    os.close();
                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                return;
            }

            jsonObject.addProperty("success", "true");
            jsonObject.addProperty("description", "user registered successfully");
            jsonObject.addProperty("password", password);

            response = jsonObject.toString();

            try {
                t.sendResponseHeaders(201, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        private void handleRegister(HttpExchange t, String inputStr) {
            Gson gson = new Gson();
            String response = "";
            JsonElement jelem = gson.fromJson(inputStr, JsonElement.class);

            if(!authorize(t)) {
                response = "Unauthorized";

                try {
                    t.sendResponseHeaders(401, response.length());
                    OutputStream os = t.getResponseBody();
                    os.write(response.getBytes());
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return;
            }

            JsonObject jobj = jelem.getAsJsonObject();

            String url = jobj.get("url").getAsString();
            String shorturl = "http://localhost:" + port + "/" + RandomStringUtils.randomAlphabetic(6);
            String redirectType = "302";// default
            if (jobj.get("redirectType") != null) {
                redirectType = jobj.get("redirectType").getAsString();
            }

            JsonObject jsonObject = new JsonObject();

            try
            {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.


                ResultSet rs = statement.executeQuery("select * from shorturls where shorturl = '" + shorturl + "'");
                // if such shorturl already exists, try another one
                while(rs.next())
                {
                    shorturl = "http://localhost:" + port + "/" + RandomStringUtils.randomAlphabetic(6);
                    rs = statement.executeQuery("select * from shorturls where shorturl = '" + shorturl + "'");
                }

                statement.executeUpdate("insert into shorturls(url, shorturl, redirect_type) values('"+ url +"', '"+ shorturl +"', '"+ redirectType +"')");

            }
            catch(SQLException e)
            {
                // it probably means no database file is found
                System.err.println("ERROR handleRegister: " + e.getMessage());

                try {

                    jsonObject.addProperty("success", "false");

                    response = jsonObject.toString();

                    t.sendResponseHeaders(500, response.length());
                    OutputStream os = t.getResponseBody();
                    os.write(response.getBytes());
                    os.close();


                } catch (IOException e1) {
                    e1.printStackTrace();
                }

                return;
            }

            jsonObject.addProperty("shortUrl", shorturl);


            response = jsonObject.toString();
            try {
                t.sendResponseHeaders(201, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }




        private void handleStatistics(HttpExchange t) {
            System.out.println("handleStatistics");
            String response = "";

            String requestURI = t.getRequestURI().toString();

            String[] result = requestURI.split("\\/");
            String accountId = null;
            if (result[1] != null) {
                accountId = result[2];
            }

            if(!authorize(t)) {
                response = "Unauthorized";

                try {
                    t.sendResponseHeaders(401, response.length());
                    OutputStream os = t.getResponseBody();
                    os.write(response.getBytes());
                    os.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }

                return;
            }

            JsonObject jsonObject = new JsonObject();
            try
            {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.

                ResultSet rs = statement.executeQuery("select * from shorturls");
                // if such shorturl already exists, try another one
                String url = null;
                int counter = 0;
                String redirectType = null;
                while(rs.next())
                {
                    url = rs.getString("url");
                    counter = rs.getInt("counter");

                    jsonObject.addProperty(url, counter);

                    System.out.println("" + counter);
                }
            }
            catch(SQLException e)
            {
                // it probably means no database file is found
                return;
            }

            try {
                response = jsonObject.toString();

                t.sendResponseHeaders(200, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        private void handleHelp(HttpExchange t) {
            System.out.println("handleHelp");

            String response = getFile("/file/help.html");

            try {
                t.sendResponseHeaders(200, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        private void handleRedirect(HttpExchange t) {
            System.out.println("handleRedirect");

            String shorturl = t.getRequestURI().toString().substring(1);
            shorturl = "http://localhost:" + port + "/" + shorturl;
            String url = null;
            int counter = 0;
            String redirectType = "302";
            String response = "Page not found";

            try
            {
                Statement statement = connection.createStatement();
                statement.setQueryTimeout(30);  // set timeout to 30 sec.

                ResultSet rs = statement.executeQuery("select * from shorturls where shorturl = '" + shorturl + "'");
                // if such shorturl already exists, try another one
                while(rs.next())
                {
                    url = rs.getString("url");
                    counter = rs.getInt("counter");
                    redirectType = rs.getString("redirect_type");
                }

                if (url != null) {
                    counter++;
                    statement.executeUpdate("UPDATE shorturls SET counter = " + counter + " where shorturl = '" + shorturl + "'");

                    try {
                        t.getResponseHeaders().add("Location", url);
                        t.sendResponseHeaders(Integer.parseInt(redirectType), 0);
                        t.getResponseBody().close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            catch(SQLException e)
            {
                // it probably means no database file is found
                System.err.println("ERROR handleRedirect: " + e.getMessage());
            }

            try {
                t.sendResponseHeaders(404, response.length());
                OutputStream os = t.getResponseBody();
                os.write(response.getBytes());
                os.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


        private String getFile(String fileName) {

            StringBuilder result = new StringBuilder("");


            InputStream is = UrlShortenerHandler.class.getResourceAsStream(fileName);
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String line;
            try {
                while ((line = reader.readLine()) != null) {
                    result.append(line);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            return result.toString();
        }


        private boolean authorize(HttpExchange t) {
            String accountId = "";
            String password = "";

            if (!t.getRequestHeaders().containsKey("Authorization")) {
                return false;
            } else {
                List<String> authHeaders = t.getRequestHeaders().get("Authorization");
                String authorization = authHeaders.get(0);

                if (authorization != null && authorization.startsWith("Basic")) {
                    // Authorization: Basic base64credentials
                    String base64Credentials = authorization.substring("Basic".length()).trim();
                    String credentials = new String(Base64.decodeBase64(base64Credentials),
                            Charset.forName("UTF-8"));
                    // credentials = username:password
                    final String[] values = credentials.split(":",2);

                    accountId = values[0];
                    password = values[1];

                    try
                    {
                        Statement statement = connection.createStatement();
                        statement.setQueryTimeout(30);  // set timeout to 30 sec.

                        ResultSet rs = statement.executeQuery("select * from account where account_id = '" + accountId + "' AND password = '" + password + "'");
                        // if such shorturl already exists, try another one

                        if (rs.next()) {
                            return true;
                        }

                    }
                    catch(SQLException e)
                    {
                        // it probably means no database file is found
                        System.err.println("ERROR authorize: " + e.getMessage());
                    }
                }
            }

            return false;
        }

    }

}
